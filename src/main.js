import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'

const app = createApp(App);
app.use(router);
app.mount('#app')

// import { createApp } from 'vue'
// import App from './App.vue'
// // import router from './router';

// new createApp({
//     // router,
//     render: h => h(App),
//   }).$mount('#app');

