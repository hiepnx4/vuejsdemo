import axios from "axios";
// import { resolve } from "core-js/fn/promise";
// import apiService from './apiService';

const urlAPI = 'http://14.177.236.88:1446'

const config = {
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
};
// export function validateUsername(username) {
//     const usernamePattern = /^[a-zA-Z0-9_-]{8,16}$/;
//     return usernamePattern.test(username);
// }
export function validateUsername(username) {
  const usernamePattern = /^0\d{9}$/;
  return usernamePattern.test(username);
}

// export function validatePassword(password) {
//     const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/;
//     return passwordPattern.test(password);
// }
export function validatePassword(password) {
  const passwordPattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[a-zA-Z\d@$!%*?&]{6,}$/;
  return passwordPattern.test(password);
}

export function refreshT() {
  const refreshToken = localStorage.getItem('refreshToken')
  let body = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
    client_id: 'Cbx_App',
  }
  let path = urlAPI + '/connect/token'
  axios.post(path, body, config)
    .then(res => {
      this.accessToken = res.data['access_token']
      this.refreshToken = res.data['refresh_token']
      localStorage.setItem('accessToken', this.accessToken)
      localStorage.setItem('refreshToken', this.refreshToken)
      alert('đã refresh token' + refreshToken)
    }).catch(err => {
      alert('Lỗi: ' + err);
      throw err;
    })
}
export const getToken = (body) => {
  // let accessTokenOld = localStorage.getItem('accessToken') ?? 0
  // let refreshTokenOld = localStorage.getItem('refreshToken') ?? 0
  // if (refreshTokenOld == 0 || refreshTokenOld == '') {
    let path = urlAPI + '/connect/token'
    return new Promise(
      resolve => {
        axios.post(path, body, config)
          .then(res => {
            resolve(res)
          }).catch(err => {
            resolve(err)
          })
      }
    )
  // } else {
  //   alert('Lỗi rồi ');
  // }
}